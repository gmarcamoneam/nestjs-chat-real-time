// import { IoAdapter } from '@nestjs/platform-socket.io';
// import * as redisIoAdapter from 'socket.io-redis';

// export class RedisIoAdapter extends IoAdapter {
//   createIOServer(port: number): any {
//     const server = super.createIOServer(port);
//     const redisAdapter = redisIoAdapter(
//       {
//         host: process.env.REDIS_HOST, 
//         port: parseInt(process.env.REDIS_PORT),
//       });
//     server.adapter(redisAdapter);
//     return server;
//   }
// }



import * as redis from 'redis';
import { Server } from 'socket.io';
import { createAdapter } from 'socket.io-redis';

const io = new Server();
const pubClient = redis.createClient({ host: 'localhost', port: 6379 });
const subClient = redis.createClient({ host: 'localhost', port: 6379 });

// Use Redis adapter with Socket.IO server
io.adapter(createAdapter({ pubClient, subClient }));

// Listen for client connections
io.on('connection', (socket) => {
  console.log(`Client ${socket.id} connected`);

  // Listen for 'message' event from client
  socket.on('message', (data) => {
    console.log(`Received message from client: ${data}`);
    // Broadcast the message to all connected clients
    io.emit('message', data);
  });

  // Listen for client disconnections
  socket.on('disconnect', () => {
    console.log(`Client ${socket.id} disconnected`);
  });
});
 